package net.azagwen.dimension_mod;

import net.minecraft.core.BlockPos;
import net.minecraft.world.phys.Vec2;
import net.minecraft.world.phys.Vec3;

import java.util.function.BiConsumer;

public class Utils {

    public static void stepThroughArea3(BlockPos origin, Vec3 from, Vec3 to, BiConsumer<BlockPos, Vec3> operation) {
        for (var x = from.x; x < to.x + 1; x++) {
            for (var y = from.y; y < to.y + 1; y++) {
                for (var z = from.z; z < to.z + 1; z++) {
                    var tempPos = origin.offset(x, y, z);
                    operation.accept(tempPos, new Vec3(x, y, z));
                }
            }
        }
    }

    public static Vec3 blockPosToVec3(BlockPos blockPos) {
        return new Vec3(blockPos.getX(), blockPos.getY(), blockPos.getZ());
    }

    public static Vec2 addVectors2(Vec2 a, Vec2 b) {
        return new Vec2(a.x + b.x, a.y + b.y);
    }

    public static Vec2 addVectors2(Vec2 a, float b) {
        return new Vec2(a.x + b, a.y + b);
    }

    public static Vec2 subVectors2(Vec2 a, Vec2 b) {
        return new Vec2(a.x - b.x, a.y - b.y);
    }

    public static Vec2 subVectors2(Vec2 a, float b) {
        return new Vec2(a.x - b, a.y - b);
    }

    public static Vec2 multiplyVectors2(Vec2 a, Vec2 b) {
        return new Vec2(a.x * b.x, a.y * b.y);
    }

    public static Vec2 multiplyVectors2(Vec2 a, float b) {
        return new Vec2(a.x * b, a.y * b);
    }

    public static Vec2 divideVectors2(Vec2 a, Vec2 b) {
        return new Vec2(a.x / b.x, a.y / b.y);
    }

    public static Vec2 divideVectors2(Vec2 a, float b) {
        return new Vec2(a.x / b, a.y / b);
    }
}
