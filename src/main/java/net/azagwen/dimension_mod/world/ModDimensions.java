package net.azagwen.dimension_mod.world;

import net.azagwen.dimension_mod.DimMod;
import net.minecraft.core.Holder;
import net.minecraft.core.Registry;
import net.minecraft.data.BuiltinRegistries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.biome.OverworldBiomeBuilder;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.dimension.DimensionType;
import net.minecraft.world.level.levelgen.*;
import net.minecraft.world.level.levelgen.synth.NormalNoise;

import java.util.LinkedHashMap;
import java.util.Map;

public class ModDimensions {
    public static final Map<DimensionRegistryKey, DimensionType> DIMENSIONS = new LinkedHashMap<>();
    public static final DimensionRegistryKey ORGANIC_REALM = makeDim("organic_realm", DimensionTypeBuilder.of().hasSkyLight().setCoordinateScale(4).setMinY(-256).setHeight(256).setLogicalHeight(256).build());
    public static final DimensionRegistryKey SYNTHETIC_REALM = makeDim("synthetic_realm", DimensionTypeBuilder.of().hasSkyLight().setCoordinateScale(4).setMinY(-256).setHeight(256).setLogicalHeight(256).build());

    // Testing
    public static final NoiseSettings ORGANIC_REALM_NOISE_SETTINGS = NoiseSettings.create(-256, 256, 1, 2);
    public static final ResourceKey<NoiseGeneratorSettings> ORGANIC_REALM_NOISE_SETTINGS_KEY = ResourceKey.create(Registry.NOISE_GENERATOR_SETTINGS_REGISTRY, DimMod.id("test"));
    public static NoiseGeneratorSettings testNoiseSettings() {
        return new NoiseGeneratorSettings(ORGANIC_REALM_NOISE_SETTINGS, Blocks.GREEN_TERRACOTTA.defaultBlockState(), Blocks.WATER.defaultBlockState(), noNewCaves(BuiltinRegistries.DENSITY_FUNCTION), lushSurface(), (new OverworldBiomeBuilder()).spawnTarget(), 63, false, true, true, false);
    }

    // From vanilla -- Testing
    private static DensityFunction getFunction(Registry<DensityFunction> registry, ResourceKey<DensityFunction> resourceKey) {
        return new DensityFunctions.HolderHolder(registry.getHolderOrThrow(resourceKey));
    }

    // From vanilla -- Testing
    private static final ResourceKey<DensityFunction> SHIFT_X = ResourceKey.create(Registry.DENSITY_FUNCTION_REGISTRY, new ResourceLocation("shift_x"));
    private static final ResourceKey<DensityFunction> SHIFT_Z = ResourceKey.create(Registry.DENSITY_FUNCTION_REGISTRY, new ResourceLocation("shift_z"));

    // From vanilla -- Testing
    private static DensityFunction postProcess(DensityFunction densityFunction) {
        DensityFunction densityFunction2 = DensityFunctions.blendDensity(densityFunction);
        return DensityFunctions.mul(DensityFunctions.interpolated(densityFunction2), DensityFunctions.constant(0.64)).squeeze();
    }

    // From vanilla -- Testing
    private static Holder<NormalNoise.NoiseParameters> getNoise(ResourceKey<NormalNoise.NoiseParameters> resourceKey) {
        return BuiltinRegistries.NOISE.getHolderOrThrow(resourceKey);
    }

    // From vanilla -- Testing
    private static NoiseRouter noNewCaves(Registry<DensityFunction> registry) {
        DensityFunction densityFunction2 = getFunction(registry, SHIFT_X);
        DensityFunction densityFunction3 = getFunction(registry, SHIFT_Z);
        DensityFunction densityFunction4 = DensityFunctions.shiftedNoise2d(densityFunction2, densityFunction3, 0.25, getNoise(Noises.TEMPERATURE));
        DensityFunction densityFunction5 = DensityFunctions.shiftedNoise2d(densityFunction2, densityFunction3, 0.25, getNoise(Noises.VEGETATION));
        return new NoiseRouter(DensityFunctions.zero(), DensityFunctions.zero(), DensityFunctions.zero(), DensityFunctions.zero(), densityFunction4, densityFunction5, DensityFunctions.zero(), DensityFunctions.zero(), DensityFunctions.zero(), DensityFunctions.zero(), DensityFunctions.zero(), DensityFunctions.zero(), DensityFunctions.zero(), DensityFunctions.zero(), DensityFunctions.zero());
    }

    // From vanilla -- Testing
    private static SurfaceRules.RuleSource makeStateRule(Block block) {
        return SurfaceRules.state(block.defaultBlockState());
    }

    // Testing
    public static SurfaceRules.RuleSource lushSurface() {
        return makeStateRule(Blocks.MOSS_BLOCK);
    }

    private static DimensionRegistryKey makeDim(String string, DimensionType dimensionType) {
        var key = new DimensionRegistryKey(DimMod.id(string));
        DIMENSIONS.put(key, dimensionType);
        return key;
    }
}
