package net.azagwen.dimension_mod.world;

import net.minecraft.util.Mth;
import net.minecraft.world.phys.Vec2;
import net.minecraft.world.phys.Vec3;

public record SdfUtils() {

    public static double merge(double shape1, double shape2) {
        return Math.min(shape1, shape2);
    }

    public static double intersect(double shape1, double shape2) {
        return Math.max(shape1, shape2);
    }

    public static double subtract(double base, double substraction) {
        return intersect(base, -substraction);
    }

    public static double interpolate(double shape1, double shape2, float amount) {
        return Mth.lerp(shape1, shape2, amount);
    }

    public static double roundMerge(double shape1, double shape2, float radius) {
        var s1 = shape1 - radius;
        var s2 = shape2 - radius;
        var i1 = Math.min(s1, 0);
        var i2 = Math.min(s2, 0);
        var intersectionSpace = new Vec2((float) i1, (float) i2);
        var insideDistance = -intersectionSpace.length();
        var simpleUnion = merge(shape1, shape2);
        var outsideDistance = Math.max(simpleUnion, radius);
        return  insideDistance + outsideDistance;
    }

    public static double roundMerge(float radius, double... shapes) {
        double prevShape = -99;
        for (var shape : shapes) {
            if (prevShape != -99) {
                prevShape = roundMerge(shape, prevShape, radius);
            } else {
                prevShape = shape;
            }
        }
        return prevShape;
    }

    public static double roundIntersect(double shape1, double shape2, float radius){
        var s1 = shape1 + radius;
        var s2 = shape2 + radius;
        var i1 = Math.max(s1, 0);
        var i2 = Math.max(s2, 0);
        var intersectionSpace = new Vec2((float) i1, (float) i2);
        var outsideDistance = intersectionSpace.length();
        var simpleIntersection = intersect(shape1, shape2);
        var insideDistance = Math.min(simpleIntersection, -radius);
        return outsideDistance + insideDistance;
    }

    public static double roundSubtract(double base, double subtraction, float radius){
        return roundIntersect(base, -subtraction, radius);
    }

    /**
     * Creates a sphere SDF for use in world Gen
     * @param p     Position to test the SDF at
     * @param s     Size of the sphere
     * @return      A double representing the density of the SDF at the given pos
     */
    public static double sphere(Vec3 p, float s) {
        return p.length() - s;
    }

    /**
     * Creates a sphere SDF for use in world Gen
     * @param p     Position to test the SDF at
     * @param s     Size of the sphere
     * @param r     Base radius of the sphere
     * @return      A double representing the density of the SDF at the given pos
     */
    public static double sphere(Vec3 p, Vec3 s, float r) {
        var newp = new Vec3(p.x / s.x, p.y / s.y, p.z / s.z);
        return sphere(newp, r);
    }

    /**
     * Creates a capped cone SDF for use in World Gen
     * @param p     Position to test the SDF at
     * @param h     Height of the cone
     * @param r1    Radius of the first end of the cone
     * @param r2    Radius of the other end of the cone
     * @return      A double representing the density of the SDF at the given pos
     */
    public static double cappedCone(Vec3 p, float h, float r1, float r2) {
        var q = new Vec2((float) p.multiply(1,0,1).length(), (float) p.y);
        var k1 = new Vec2(r2, h);
        var k2 = new Vec2(r2 - r1,2.0F * h);
        var ca = new Vec2(q.x - Math.min(q.x, (q.y < 0.0) ? r1 : r2), Math.abs(q.y) - h);
        var cb1 = q.add(k1.scale(-1));
        var cb2 = k2
                .scale(Mth.clamp(k1
                                .add(q.scale(-1))
                                .dot(k2)
                                /k2.dot(k2),
                        0, 1));
        var cb = cb1.add(cb2);
        var s = ((cb.x < 0.0 && ca.y < 0.0) ? -1.0 : 1.0);
        return s * Mth.sqrt(Math.min(ca.dot(ca), cb.dot(cb)));
    }

    /**
     * Creates a round cone SDF for use in World Gen
     * @param p     Position to test the SDF at
     * @param h     Height of the cone
     * @param r1    Radius of the first end of the cone
     * @param r2    Radius of the other end of the cone
     * @return      A double representing the density of the SDF at the given pos
     */
    public static double roundCone(Vec3 p, float r1, float r2, float h)
    {
        // sampling independent computations (only depend on shape)
        var b = (r1 - r2) / h;
        var a = (float) Math.sqrt(1.0F - b * b);

        // sampling dependant computations
        var q1 = new Vec2((float) p.x, (float) p.z);
        var q2 = new Vec2(q1.length(), (float) p.y);

        var k1 = new Vec2(-b, a);
        var k2 = q2.dot(k1);

        if( k2 < 0.0 ) return q2.length() - r1;
        if( k2 > a*h ) return q2.add(new Vec2(0F, -h)).length() - r2;
        return q2.dot(new Vec2(a,b) ) - r1;
    }
}
