package net.azagwen.dimension_mod.world;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.util.valueproviders.IntProvider;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProvider;

public record AncientAzaleaTreeFeatureConfig(IntProvider bottomTrunkRadius, IntProvider trunkHeight, BlockStateProvider trunkBlock, BlockStateProvider leafBlock, boolean debug) implements FeatureConfiguration {
    public static final Codec<AncientAzaleaTreeFeatureConfig> CODEC = RecordCodecBuilder.create(instance -> instance.group(
            IntProvider.CODEC.fieldOf("bottom_trunk_radius").forGetter(AncientAzaleaTreeFeatureConfig::bottomTrunkRadius),
            IntProvider.CODEC.fieldOf("trunk_height").forGetter(AncientAzaleaTreeFeatureConfig::trunkHeight),
            BlockStateProvider.CODEC.fieldOf("trunk_block").forGetter(AncientAzaleaTreeFeatureConfig::trunkBlock),
            BlockStateProvider.CODEC.fieldOf("leaf_block").forGetter(AncientAzaleaTreeFeatureConfig::leafBlock),
            Codec.BOOL.fieldOf("debug").forGetter(AncientAzaleaTreeFeatureConfig::debug)
    ).apply(instance, instance.stable(AncientAzaleaTreeFeatureConfig::new)));
}