package net.azagwen.dimension_mod.world;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.util.valueproviders.UniformInt;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.dimension.BuiltinDimensionTypes;
import net.minecraft.world.level.dimension.DimensionDefaults;
import net.minecraft.world.level.dimension.DimensionType;

import java.util.OptionalLong;

public class DimensionTypeBuilder {
    private OptionalLong fixedTime;
    private boolean hasSkyLight;
    private boolean hasCeiling;
    private boolean ultraWarm;
    private boolean natural;
    private double coordinateScale;
    private boolean bedWorks;
    private boolean respawnAnchorWorks;
    private int minY;
    private int height;
    private int logicalHeight;
    private TagKey<Block> infiniburn;
    private ResourceLocation effectsLocation;
    private float ambientLight;
    private DimensionType.MonsterSettings monsterSettings;

    private DimensionTypeBuilder() {
        this.fixedTime = OptionalLong.empty();
        this.hasSkyLight = false;
        this.hasCeiling = false;
        this.ultraWarm = false;
        this.natural = false;
        this.coordinateScale = 1; // Overworld value
        this.bedWorks = false;
        this.respawnAnchorWorks = false;
        this.minY = DimensionDefaults.OVERWORLD_MIN_Y; // Overworld value
        this.height = DimensionDefaults.OVERWORLD_LEVEL_HEIGHT; // Overworld value
        this.logicalHeight = DimensionDefaults.OVERWORLD_LOGICAL_HEIGHT; // Overworld value
        this.infiniburn = BlockTags.INFINIBURN_OVERWORLD; // Overworld value
        this.effectsLocation = BuiltinDimensionTypes.OVERWORLD_EFFECTS; // Overworld value
        this.ambientLight = 0.0F; // Overworld value
        this.monsterSettings = new DimensionType.MonsterSettings(false, true, UniformInt.of(0, 7), 0); // Overworld value
    }

    public static DimensionTypeBuilder of() {
        return new DimensionTypeBuilder();
    }

    public DimensionTypeBuilder fixedTime(OptionalLong fixedTime) {
        this.fixedTime = fixedTime;
        return this;
    }

    public DimensionTypeBuilder hasSkyLight() {
        this.hasSkyLight = true;
        return this;
    }

    public DimensionTypeBuilder hasCeiling() {
        this.hasCeiling = true;
        return this;
    }

    public DimensionTypeBuilder ultraWarm() {
        this.ultraWarm = true;
        return this;
    }

    public DimensionTypeBuilder natural() {
        this.natural = true;
        return this;
    }

    public DimensionTypeBuilder setCoordinateScale(double coordinateScale) {
        this.coordinateScale = coordinateScale;
        return this;
    }

    public DimensionTypeBuilder bedWorks() {
        this.bedWorks = true;
        return this;
    }

    public DimensionTypeBuilder respawnAnchorWorks() {
        this.respawnAnchorWorks = true;
        return this;
    }

    public DimensionTypeBuilder setMinY(int minY) {
        this.minY = minY;
        return this;
    }

    public DimensionTypeBuilder setHeight(int height) {
        this.height = height;
        return this;
    }

    public DimensionTypeBuilder setLogicalHeight(int logicalHeight) {
        this.logicalHeight = logicalHeight;
        return this;
    }

    public DimensionTypeBuilder setInfiniBurn(TagKey<Block> infiniBurn) {
        this.infiniburn = infiniBurn;
        return this;
    }

    public DimensionTypeBuilder setEffectsLocation(ResourceLocation effectsLocation) {
        this.effectsLocation = effectsLocation;
        return this;
    }

    public DimensionTypeBuilder setAmbientLight(float ambientLight) {
        this.ambientLight = ambientLight;
        return this;
    }

    public DimensionTypeBuilder setMonsterSettings(DimensionType.MonsterSettings monsterSettings) {
        this.monsterSettings = monsterSettings;
        return this;
    }

    public DimensionType build() {
        return new DimensionType(
                this.fixedTime,
                this.hasSkyLight,
                this.hasCeiling,
                this.ultraWarm,
                this.natural,
                this.coordinateScale,
                this.bedWorks,
                this.respawnAnchorWorks,
                this.minY,
                this.height,
                this.logicalHeight,
                this.infiniburn,
                this.effectsLocation,
                this.ambientLight,
                this.monsterSettings
        );
    }
}
