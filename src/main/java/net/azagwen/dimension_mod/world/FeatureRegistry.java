package net.azagwen.dimension_mod.world;

import com.google.common.collect.Lists;
import net.azagwen.dimension_mod.DimMod;
import net.fabricmc.fabric.api.biome.v1.BiomeModifications;
import net.fabricmc.fabric.api.biome.v1.BiomeSelectors;
import net.fabricmc.fabric.api.biome.v1.ModificationPhase;
import net.minecraft.core.Direction;
import net.minecraft.core.Holder;
import net.minecraft.core.Registry;
import net.minecraft.data.BuiltinRegistries;
import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.resources.ResourceKey;
import net.minecraft.util.random.SimpleWeightedRandomList;
import net.minecraft.util.valueproviders.ConstantInt;
import net.minecraft.util.valueproviders.UniformInt;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.blockpredicates.BlockPredicate;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProvider;
import net.minecraft.world.level.levelgen.feature.stateproviders.WeightedStateProvider;
import net.minecraft.world.level.levelgen.placement.*;

public class FeatureRegistry {
    public static int[] FEATURE_COUNT = new int[] {0, 0, 0}; // Feature, ConfiguredFeature, PlacedFeature
    public static final Feature<AncientAzaleaTreeFeatureConfig> ANCIENT_AZALEA_TREE = registerFeature("ancient_azalea_tree", new AncientAzaleaTreeFeature(AncientAzaleaTreeFeatureConfig.CODEC));

    public static class ConfiguredFeatures {
        public static final ConfiguredFeature<?, ?> CONFIGURED_ANCIENT_AZALEA_TREE = registerConfiguredFeature("ancient_azalea_tree", new ConfiguredFeature<>(ANCIENT_AZALEA_TREE, new AncientAzaleaTreeFeatureConfig(
                UniformInt.of(5, 7),
                UniformInt.of(10, 15),
                BlockStateProvider.simple(Blocks.OAK_LOG),
                new WeightedStateProvider(new SimpleWeightedRandomList.Builder<BlockState>()
                        .add(Blocks.AZALEA_LEAVES.defaultBlockState(), 10)
                        .add(Blocks.FLOWERING_AZALEA_LEAVES.defaultBlockState(), 2)
                        .build()
                ),
                false
        )));

        /**
         * Registers a Configured Feature
         * @param name                  Name of teh feature
         * @param configuredFeature     Configured Feature to be registered
         * @return                      Now registered Configured feature
         * @param <FC>                  The type of the {@link FeatureConfiguration}
         * @param <F>                   The type of the {@link Feature}
         */
        private static <FC extends FeatureConfiguration, F extends Feature<FC>> ConfiguredFeature<FC, F> registerConfiguredFeature(String name, ConfiguredFeature<FC, F> configuredFeature) {
            FEATURE_COUNT[1]++;
            return Registry.register(BuiltinRegistries.CONFIGURED_FEATURE, DimMod.id(name), configuredFeature);
        }
    }

    public static class PlacedFeatures {
        public static final PlacedFeature PLACED_ANCIENT_AZALEA_TREE = registerPlacedFeature("ancient_azalea_tree", new PlacedFeature(
                Holder.direct(ConfiguredFeatures.CONFIGURED_ANCIENT_AZALEA_TREE),
                Lists.newArrayList(
                        CountPlacement.of(UniformInt.of(5, 10)),
                        PlacementUtils.RANGE_BOTTOM_TO_MAX_TERRAIN_HEIGHT,
                        EnvironmentScanPlacement.scanningFor(Direction.DOWN, BlockPredicate.solid(), BlockPredicate.ONLY_IN_AIR_PREDICATE, 12),
                        RarityFilter.onAverageOnceEvery(3),
                        RandomOffsetPlacement.horizontal(ConstantInt.of(5)),
                        BiomeFilter.biome()
                )
        ));

        /**
         *
         * @param name              Name of teh feature
         * @param placedFeature     Placed Feature to be registered
         * @return                  Now registered Placed feature
         */
        private static PlacedFeature registerPlacedFeature(String name, PlacedFeature placedFeature) {
            FEATURE_COUNT[2]++;
            return Registry.register(BuiltinRegistries.PLACED_FEATURE, DimMod.id(name), placedFeature);
        }
    }

    /**
     * Registers a base feature
     * @param name      Name of teh feature
     * @param feature   Feature to be registered
     * @return          Now registered feature
     * @param <FC>      The type of the {@link FeatureConfiguration}
     * @param <F>       The type of the {@link Feature}
     */
    private static <FC extends FeatureConfiguration, F extends Feature<FC>> F registerFeature(String name, F feature) {
        FEATURE_COUNT[0]++;
        return Registry.register(Registry.FEATURE, DimMod.id(name), feature);
    }

    /**
     * Adds a placed feature to a vanilla biome
     * @param modificationName  Name of the modification
     * @param biome             Resource key of the biome to add a feature to
     * @param step              Decoration step at which the feature will be added
     * @param feature           Placed feature to add t othe biome
     */
    private static void addBuiltinFeatureToVanillaBiome(String modificationName, ResourceKey<Biome> biome, GenerationStep.Decoration step, PlacedFeature feature) {
        BiomeModifications.create(DimMod.id(modificationName)).add(ModificationPhase.ADDITIONS, BiomeSelectors.includeByKey(biome),
                (context) -> context.getGenerationSettings().addBuiltInFeature(step, feature)
        );
    }

    public static void init() {
        addBuiltinFeatureToVanillaBiome("add_ancient_azalea_to_lush_caves", Biomes.LUSH_CAVES, GenerationStep.Decoration.VEGETAL_DECORATION, PlacedFeatures.PLACED_ANCIENT_AZALEA_TREE);

        // Registry message
        var featureCount = FEATURE_COUNT[0];
        var configuredFeatureCount = FEATURE_COUNT[1];
        var placedFeatureCount = FEATURE_COUNT[2];

        if (featureCount > 0 || configuredFeatureCount > 0 || placedFeatureCount > 0) {
            var isFeaturePlural = featureCount > 1 ? "s" : "";
            var isConfiguredFeaturePlural = configuredFeatureCount > 1 ? "s" : "";
            var isPlacedFeaturePlural = placedFeatureCount > 1 ? "s" : "";
            DimMod.LOGGER.info("Registered {} feature{}, {} configured feature{} and {} placed feature{}", featureCount, isFeaturePlural, configuredFeatureCount, isConfiguredFeaturePlural, placedFeatureCount, isPlacedFeaturePlural);
        }
    }
}
