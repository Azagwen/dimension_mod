package net.azagwen.dimension_mod.world;

import com.mojang.serialization.Codec;
import net.azagwen.dimension_mod.DimMod;
import net.minecraft.core.Registry;
import net.minecraft.world.level.chunk.ChunkGenerator;

public class ModChunkGenerators {

    private static Codec<? extends ChunkGenerator> registerGenerator(Registry<Codec<? extends ChunkGenerator>> registry, String name, Codec<? extends ChunkGenerator> codec) {
        return Registry.register(registry, DimMod.id(name), codec);
    }

    public static void register(Registry<Codec<? extends ChunkGenerator>> registry) {

        // Example vanilla values, all generators should be registered similarly
        // registerGenerator(registry, "noise", NoiseBasedChunkGenerator.CODEC);
        // registerGenerator(registry, "flat", FlatLevelSource.CODEC);
        // registerGenerator(registry, "debug", DebugLevelSource.CODEC);
    }
}
