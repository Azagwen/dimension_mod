package net.azagwen.dimension_mod.world;

import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.dimension.DimensionType;
import net.minecraft.world.level.dimension.LevelStem;

public class DimensionRegistryKey {
    private final ResourceKey<Level> levelKey;
    private final ResourceKey<DimensionType> typeKey;
    private final ResourceKey<LevelStem> stemKey;

    public DimensionRegistryKey(ResourceLocation id) {
        this.levelKey = ResourceKey.create(Registry.DIMENSION_REGISTRY, id);
        this.typeKey = ResourceKey.create(Registry.DIMENSION_TYPE_REGISTRY, id);
        this.stemKey = ResourceKey.create(Registry.LEVEL_STEM_REGISTRY, id);
    }

    public ResourceKey<Level> getLevelKey() {
        return levelKey;
    }

    public ResourceKey<DimensionType> getTypeKey() {
        return typeKey;
    }

    public ResourceKey<LevelStem> getStemKey() {
        return stemKey;
    }
}
