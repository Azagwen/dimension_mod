package net.azagwen.dimension_mod.world;

import com.mojang.serialization.Codec;
import net.azagwen.dimension_mod.Utils;
import net.azagwen.dimension_mod.libraries.FastNoiseLite;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.phys.Vec3;

public class AncientAzaleaTreeFeature extends Feature<AncientAzaleaTreeFeatureConfig> {

    public AncientAzaleaTreeFeature(Codec<AncientAzaleaTreeFeatureConfig> codec) {
        super(codec);
    }

    @Override
    public boolean place(FeaturePlaceContext<AncientAzaleaTreeFeatureConfig> context) {
        var pos = context.origin();
        var config = context.config();
        int bottomRadius = config.bottomTrunkRadius().sample(context.random());
        int topRadius = bottomRadius - (bottomRadius == 5 ? 3 : 4);
        int height = config.trunkHeight().sample(context.random());
        var noise = new FastNoiseLite(context.random().nextInt(1000, 2000));

        // Check if the trunk has space to spawn, if not, return early
         for (var i = pos.getY() + (height * 0.5); i < height; i++) {
             if (!context.level().getBlockState(new BlockPos(pos.getX(), i, pos.getZ())).isAir()) {
                 return false;
             }
         }

        this.placeTrunk(bottomRadius, topRadius, height, noise, context);
        this.placeLeaves(bottomRadius, height, noise, context);

        return true;
    }

    // Method to place the leaves shape
    private void placeLeaves(int bottomRadius, int height, FastNoiseLite noise, FeaturePlaceContext<AncientAzaleaTreeFeatureConfig> context) {
        var origin = context.origin();
        var config = context.config();
        var radius = bottomRadius + 3;

        for (var newPos : BlockPos.betweenClosed(origin.offset(-radius * 2, 0, -radius * 2), origin.offset(radius * 2, height * 2, radius * 2))) {
            var posVec = Utils.blockPosToVec3(origin.subtract(newPos).offset(0, height, 0));
            var vec = new FastNoiseLite.Vector3((float) posVec.x, (float) posVec.y, (float) posVec.z);

            // Setup our noise & displace the coordinates with it
            noise.SetNoiseType(FastNoiseLite.NoiseType.Perlin);
            noise.SetFrequency(0.1F);
            noise.SetDomainWarpAmp(6.0F);
            noise.DomainWarp(vec);
            posVec = new Vec3(vec.x, vec.y, vec.z).scale(1.25); // Scale medium up to account for smoothing

            // Create the SDF
            var outerSphere = SdfUtils.sphere(posVec, new Vec3(1.75, 1, 1.75), radius);
            var innerSphere = SdfUtils.sphere(posVec.add(0, -5, 0), new Vec3(1.75, 0.75, 1.75), radius);
            var finalSphere = SdfUtils.roundSubtract(outerSphere - 3, innerSphere - 3, 5); // Substract 3 units to smooth shapes

            // Place blocks inside the SDF
            if (finalSphere <= 0.5F) {
                if (context.level().getBlockState(newPos).getMaterial().isReplaceable()){
                    this.placeBlock(newPos, origin, config.leafBlock().getState(context.random(), newPos), context.level(), config);
                }
            }
        }
    }

    // Method to place the trunk shape
    private void placeTrunk(int bottomRadius, int topRadius, int height, FastNoiseLite noise, FeaturePlaceContext<AncientAzaleaTreeFeatureConfig> context) {
        var origin = context.origin();
        var config = context.config();

        for (var newPos : BlockPos.betweenClosed(origin.offset(-bottomRadius, -bottomRadius, -bottomRadius), origin.offset(bottomRadius, height, bottomRadius))) {
            var posVec = Utils.blockPosToVec3(origin.subtract(newPos));
            var vec = new FastNoiseLite.Vector3((float) posVec.x, (float) posVec.y, (float) posVec.z);

            // Setup our noise & displace the coordinates with it
            noise.SetNoiseType(FastNoiseLite.NoiseType.Perlin);
            noise.SetFrequency(0.1F);
            noise.SetDomainWarpAmp(3.0F);
            noise.DomainWarp(vec);
            posVec = new Vec3(vec.x, posVec.y, vec.z);

            // Create the SDF
            var outerCone = SdfUtils.cappedCone(posVec, height, topRadius, bottomRadius);
            var innerCone = SdfUtils.cappedCone(posVec, height - 2, topRadius - 3, bottomRadius - 2);
            var finalCone = SdfUtils.subtract(outerCone, innerCone);

            // Place blocks inside the SDF
            if (finalCone <= 0) {
                this.placeBlock(newPos, origin, config.trunkBlock().getState(context.random(), newPos), context.level(), config);
            }
        }
    }

    private void placeBlock(BlockPos newPos, BlockPos pos, BlockState block, WorldGenLevel level, AncientAzaleaTreeFeatureConfig config) {
        if (config.debug()) {
            this.placeDebugBlocks(newPos, pos, level);
        } else {
            this.setBlock(level, newPos, block);
        }
    }

    private void placeDebugBlocks(BlockPos newPos, BlockPos pos, WorldGenLevel level) {
        if (newPos.getX() == pos.getX() && newPos.getZ() == pos.getZ()) {
            this.setBlock(level, newPos, Blocks.GLOWSTONE.defaultBlockState());
        } else {
            this.setBlock(level, newPos, Blocks.GLASS.defaultBlockState());
        }
    }
}
