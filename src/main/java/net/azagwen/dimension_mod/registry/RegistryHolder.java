package net.azagwen.dimension_mod.registry;

import com.google.common.collect.Maps;
import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceLocation;

import java.util.Map;
import java.util.function.Supplier;

/**
 * Utility class that replicates a very simplified version of Forge's DefferedRegistry Util
 * @param <T>   the Kind of object we want to store in this
 */
public class RegistryHolder<T> {
    private final Map<String, Supplier<? extends T>> objectsToRegister = Maps.newLinkedHashMap();
    private final Registry<T> registry;
    private final String modId;

    private RegistryHolder(Registry<T> registry, String modId) {
        this.registry = registry;
        this.modId = modId;
    }

    public Map<String, Supplier<? extends T>> getEntries() {
        return this.objectsToRegister;
    }

    public static <T> RegistryHolder<T> create(Registry<T> registry, String modId) {
        return new RegistryHolder<>(registry, modId);
    }

    public <I extends T> Supplier<? extends I> register(String name, Supplier<? extends I> object) {
        objectsToRegister.put(name, object);
        return object;
    }

    public void register() {
        this.objectsToRegister.forEach((name, object) -> Registry.register(this.registry, new ResourceLocation(this.modId, name), object.get()));
    }
}
