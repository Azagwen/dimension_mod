package net.azagwen.dimension_mod.mixin;

import net.azagwen.dimension_mod.world.ModDimensions;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.dimension.DimensionType;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.nio.file.Path;

@Mixin(DimensionType.class)
public class AddDimensionTypes {

    @Inject(method = "getStorageFolder", at = @At("HEAD"), cancellable = true)
    private static void getStorageFolder(ResourceKey<Level> resourceKey, Path path, CallbackInfoReturnable<Path> cir) {
        if (resourceKey == ModDimensions.ORGANIC_REALM.getLevelKey()) {
            cir.setReturnValue(path.resolve("DIM_MOD-ORGANIC_REALM"));
        }
        if (resourceKey == ModDimensions.SYNTHETIC_REALM.getLevelKey()) {
            cir.setReturnValue(path.resolve("DIM_MOD-SYNTHETIC_REALM"));
        }
    }
}
