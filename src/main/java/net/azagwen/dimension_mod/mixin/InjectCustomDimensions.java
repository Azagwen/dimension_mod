package net.azagwen.dimension_mod.mixin;


import com.mojang.serialization.Codec;
import net.azagwen.dimension_mod.world.ModChunkGenerators;
import net.azagwen.dimension_mod.world.ModDimensions;
import net.minecraft.core.Holder;
import net.minecraft.core.Registry;
import net.minecraft.data.BuiltinRegistries;
import net.minecraft.data.worldgen.DimensionTypes;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.chunk.ChunkGenerators;
import net.minecraft.world.level.dimension.DimensionType;
import net.minecraft.world.level.levelgen.NoiseGeneratorSettings;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

public class InjectCustomDimensions {

    @Mixin(DimensionTypes.class)
    private static class registerDimensionTypes {

        @Inject(method = "bootstrap", at = @At("HEAD"))
        private static void bootStrap(Registry<DimensionType> registry, CallbackInfoReturnable<Holder<DimensionType>> cir) {
            for (var typeKey : ModDimensions.DIMENSIONS.entrySet()){
                BuiltinRegistries.register(registry, typeKey.getKey().getTypeKey(), typeKey.getValue());
            }
        }
    }

    // Testing
    @Mixin(NoiseGeneratorSettings.class)
    private static abstract class registerNoiseSettings {

        @Shadow
        private static Holder<NoiseGeneratorSettings> register(Registry<NoiseGeneratorSettings> registry, ResourceKey<NoiseGeneratorSettings> resourceKey, NoiseGeneratorSettings noiseGeneratorSettings) {
            return null;
        }

        @Inject(method = "bootstrap", at = @At("HEAD"))
        private static void bootStrap(Registry<NoiseGeneratorSettings> registry, CallbackInfoReturnable<Holder<NoiseGeneratorSettings>> cir) {
            register(registry, ModDimensions.ORGANIC_REALM_NOISE_SETTINGS_KEY, ModDimensions.testNoiseSettings());
        }
    }

    @Mixin(ChunkGenerators.class)
    private static class registerChunkGenerators {

        @Inject(method = "bootstrap", at = @At("HEAD"))
        private static void bootStrap(Registry<Codec<? extends ChunkGenerator>> registry, CallbackInfoReturnable<Codec<? extends ChunkGenerator>> cir) {
            ModChunkGenerators.register(registry);
        }
    }
}
