package net.azagwen.dimension_mod.mixin;

import net.azagwen.dimension_mod.events.RightClickBlockEvent;
import net.minecraft.client.multiplayer.MultiPlayerGameMode;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.level.ServerPlayerGameMode;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.BlockHitResult;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

public class InjectRightClickOnBlockEvent {

    @Mixin(ServerPlayerGameMode.class)
    private static class ServerEvent {

        @Inject(method = "useItemOn", at = @At("HEAD"), cancellable = true)
        private void hookUseOnEvent(ServerPlayer serverPlayer, Level level, ItemStack itemStack, InteractionHand interactionHand, BlockHitResult blockHitResult, CallbackInfoReturnable<InteractionResult> cir) {
            var result = RightClickBlockEvent.EVENT.invoker().interact(new UseOnContext(serverPlayer, interactionHand, blockHitResult));

            if(result == InteractionResult.FAIL) {
                cir.cancel();
            }
        }
    }

    @Mixin(MultiPlayerGameMode.class)
    private static class ClientEvent {

        @Inject(method = "useItemOn", at = @At("HEAD"), cancellable = true)
        private void hookUseOnEvent(LocalPlayer localPlayer, InteractionHand interactionHand, BlockHitResult blockHitResult, CallbackInfoReturnable<InteractionResult> cir) {
            var result = RightClickBlockEvent.EVENT.invoker().interact(new UseOnContext(localPlayer, interactionHand, blockHitResult));

            if(result == InteractionResult.FAIL) {
                cir.cancel();
            }
        }
    }
}
