package net.azagwen.dimension_mod.item;

import net.azagwen.dimension_mod.DimMod;
import net.azagwen.dimension_mod.block.BlockRegistry;
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;

public class ModCreativeTabs {

    public static final CreativeModeTab MAIN_TAB = FabricItemGroupBuilder.create(DimMod.id("dim_mod_tab")).icon(() -> new ItemStack(BlockRegistry.TILLED_MOSS_BLOCK)).build();
}
