package net.azagwen.dimension_mod.item;

import net.azagwen.dimension_mod.DimMod;
import net.minecraft.core.Registry;
import net.minecraft.world.item.Item;

public class ItemRegistry {
    public static int ITEM_COUNT = 0;
    public static final Item AZALEA_FLOWER = registerItem("azalea_flower", new Item(new Item.Properties().tab(ModCreativeTabs.MAIN_TAB)));
    public static final Item RAW_AZALEA_GOLD = registerItem("raw_azalea_gold", new Item(new Item.Properties().tab(ModCreativeTabs.MAIN_TAB)));
    public static final Item AZALEA_GOLD_INGOT = registerItem("azalea_gold_ingot", new Item(new Item.Properties().tab(ModCreativeTabs.MAIN_TAB)));
    public static final Item AZALEA_GOLD_NUGGET = registerItem("azalea_gold_nugget", new Item(new Item.Properties().tab(ModCreativeTabs.MAIN_TAB)));

    public static Item registerItem(String name, Item item) {
        ITEM_COUNT++;
        return Registry.register(Registry.ITEM, DimMod.id(name), item);
    }

    public static void init() {
        if (ITEM_COUNT > 0){
            DimMod.LOGGER.info("Registered {} item{}", ITEM_COUNT, ITEM_COUNT > 1 ? "s" : "");
        }
    }
}
