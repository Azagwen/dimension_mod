package net.azagwen.dimension_mod;

import net.azagwen.dimension_mod.block.BlockRegistry;
import net.azagwen.dimension_mod.item.ItemRegistry;
import net.azagwen.dimension_mod.world.FeatureRegistry;
import net.fabricmc.api.ModInitializer;
import net.minecraft.resources.ResourceLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DimMod implements ModInitializer {
    public static final String MOD_ID = "dimension_mod";
    public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);

    public static ResourceLocation id(String path) {
        return new ResourceLocation(MOD_ID, path);
    }

    @Override
    public void onInitialize() {
        BlockRegistry.init();
        ItemRegistry.init();
        FeatureRegistry.init();

        // Event registration
        WorldEvents.register();

        // Misc & Debug
    }
}
