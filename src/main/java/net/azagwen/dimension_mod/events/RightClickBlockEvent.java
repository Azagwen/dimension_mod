package net.azagwen.dimension_mod.events;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.item.context.UseOnContext;

import java.util.concurrent.atomic.AtomicReference;

public interface RightClickBlockEvent {
    AtomicReference<Integer> EVENTS = new AtomicReference<>(0);

    Event<RightClickBlockEvent> EVENT = EventFactory.createArrayBacked(RightClickBlockEvent.class, (listeners) -> (context) -> {
            for (RightClickBlockEvent listener : listeners) {
                var result = listener.interact(context);

                if(result != InteractionResult.PASS) {
                    return result;
                }
            }
        return InteractionResult.PASS;
        });

    InteractionResult interact(UseOnContext context);
    static void register(RightClickBlockEvent listener) {
        EVENTS.set(EVENTS.get() + 1);
        EVENT.register(listener);
    }
}
