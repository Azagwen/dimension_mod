package net.azagwen.dimension_mod.block;

import net.azagwen.dimension_mod.Utils;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;

import java.util.concurrent.atomic.AtomicBoolean;

public class AzaleaGoldBlock extends Block {

    public AzaleaGoldBlock(Properties properties) {
        super(properties);
    }

    // Very pwetty portal shape graph uwu
    // |--------|--------|--------|--------|--------|
    // |  2,  2 |  1,  2 |  0,  2 | -1,  2 | -2,  2 |
    // |        |        |        |        |        |
    // |--------|--------|--------|--------|--------|
    // |  2,  1 |  1,  1 |  0,  1 | -1,  1 | -2,  1 |
    // |        |        |        |        |        |
    // |--------|--------|--------|--------|--------|
    // |  2,  0 |  1,  0 |  0,  0 | -1,  0 | -2,  0 |
    // |        |        |        |        |        |
    // |--------|--------|--------|--------|--------|
    // |  2, -1 |  1, -1 |  0, -1 | -1, -1 | -2, -1 |
    // |        |        |        |        |        |
    // |--------|--------|--------|--------|--------|
    // |  2, -2 |  1, -2 |  0, -2 | -1, -2 | -2, -2 |
    // |        |        |        |        |        |
    // |--------|--------|--------|--------|--------|
    @Override
    public void onPlace(BlockState pState, Level pLevel, BlockPos pPos, BlockState pOldState, boolean pIsMoving) {

        // Check for possible portal origins in a 3x3 area around the trunkBlock
        Utils.stepThroughArea3(pPos, new Vec3(-1, 0, -1), new Vec3(1, 0, 1), (possibleIgnitionPos, l) -> {
            var canPortalSpawn = new AtomicBoolean(true);

            // Look for required frame blocks for this trunkBlock to ignite the Lush portal
            Utils.stepThroughArea3(possibleIgnitionPos, new Vec3(-2, 0, -2), new Vec3(2, 0, 2), (possibleFramePos, loopStatus) -> {
                var x = loopStatus.x();
                var z = loopStatus.z();

                // Check the search area edges for frame blocks
                if (Math.abs(x) == 2 || Math.abs(z) == 2) {
                    var stateUp = pLevel.getBlockState(possibleFramePos.above());
                    var isCurrentPosFrame = pLevel.getBlockState(possibleFramePos).is(BlockRegistry.TILLED_MOSS_BLOCK);
                    var isCurrentPosCarrot = stateUp.is(BlockRegistry.GOLDEN_CARROTS) && stateUp.getValue(GoldenCarrotBlock.AGE) == 7;

                    // If a trunkBlock isn't found, the ignition is instantly cancelled
                    if (!isCurrentPosFrame || !isCurrentPosCarrot) {
                        canPortalSpawn.set(false);
                    }
                }
            });
            // If all frame blocks have been validated, spawn portal
            if (canPortalSpawn.get()) {
                Utils.stepThroughArea3(possibleIgnitionPos, new Vec3(-1, 0, -1), new Vec3(1, 0, 1), (portalPos, portalLoopStatus) -> {
                    var portalState = BlockRegistry.LUSH_PORTAL.defaultBlockState();
                    pLevel.setBlock(portalPos, portalState.setValue(LushPortalBlock.POS_X, (int) portalLoopStatus.x + 1).setValue(LushPortalBlock.POS_Z, (int) portalLoopStatus.z + 1), 3);
                });
            }
        });
    }
}
