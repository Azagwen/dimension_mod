package net.azagwen.dimension_mod.block;

import net.azagwen.dimension_mod.DimMod;
import net.azagwen.dimension_mod.item.ItemRegistry;
import net.azagwen.dimension_mod.item.ModCreativeTabs;
import net.minecraft.core.Registry;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.material.Material;

import java.util.function.BiFunction;

public class BlockRegistry {
    public static int BLOCK_COUNT = 0;
    public static final Block TILLED_MOSS_BLOCK = registerBlock("tilled_moss_block", new TilledMossBlock(BlockBehaviour.Properties.copy(Blocks.MOSS_BLOCK).randomTicks()), ModCreativeTabs.MAIN_TAB);
    public static final Block GOLDEN_CARROTS = registerBlock("golden_carrots", new GoldenCarrotBlock(BlockBehaviour.Properties.of(Material.PLANT).noCollission().randomTicks().instabreak().sound(SoundType.CROP)));
    public static final Block AZALEA_GOLD_BLOCK = registerBlock("azalea_gold_block", new AzaleaGoldBlock(BlockBehaviour.Properties.of(Material.METAL).sound(SoundType.METAL)), ModCreativeTabs.MAIN_TAB);
    public static final Block LUSH_PORTAL = registerBlock("lush_portal", new LushPortalBlock(BlockBehaviour.Properties.of(Material.PORTAL).sound(SoundType.AMETHYST).emissiveRendering((state, getter, pos) -> true)));

    /**
     * Register a trunkBlock
     * @param name      Name of the trunkBlock
     * @param block     The Block
     * @return          The resulting registered trunkBlock
     * @param <B>       The type of the trunkBlock
     */
    private static <B extends Block> B registerBlock(String name, B block) {
        BLOCK_COUNT++;
        return Registry.register(Registry.BLOCK, DimMod.id(name), block);
    }

    /**
     * Register a trunkBlock with a fully customized item
     * @param name                  Name of the trunkBlock
     * @param block                 The Block
     * @param blockItemFunc         {@link BiFunction} for creating a new {@link BlockItem}
     * @param itemProperties        The properties of the Block Item
     * @param tab                   The creative tab the item should appear in
     * @return                      The resulting registered trunkBlock
     * @param <B>                   The type of the trunkBlock
     * @param <I>                   The type of the item
     */
    private static <B extends Block, I extends Item> B registerBlock(String name, B block, BiFunction<Block, Item.Properties, I> blockItemFunc, Item.Properties itemProperties, CreativeModeTab tab) {
        var result = registerBlock(name, block);
        ItemRegistry.registerItem(name, blockItemFunc.apply(result, itemProperties.tab(tab)));
        return result;
    }

    /**
     * Registers a trunkBlock with a simple {@link BlockItem}
     * @param name      Name of the trunkBlock
     * @param block     The Block
     * @param tab       The Creative tab the item should appear in
     * @return          The resulting registered Block
     * @param <B>       The type of the trunkBlock
     */
    private static <B extends Block> B registerBlock(String name, B block, CreativeModeTab tab) {
        var result = registerBlock(name, block);
        ItemRegistry.registerItem(name, new BlockItem(result, new Item.Properties().tab(tab)));
        return result;
    }

    public static void init() {
        if (BLOCK_COUNT > 0){
            DimMod.LOGGER.info("Registered {} trunkBlock{}", BLOCK_COUNT, BLOCK_COUNT > 1 ? "s" : "");
        }
    }
}
