package net.azagwen.dimension_mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.Shapes;

public class AbstractPortalBlock extends Block {
    private final ResourceKey<Level> destinationDim;

    public AbstractPortalBlock(Properties properties, ResourceKey<Level> destinationDim) {
        super(properties);
        this.destinationDim = destinationDim;
    }

    @Override
    public void entityInside(BlockState pState, Level pLevel, BlockPos pPos, Entity pEntity) {
        if (pLevel instanceof ServerLevel && !pEntity.isPassenger() && !pEntity.isVehicle() && pEntity.canChangeDimensions() && Shapes.joinIsNotEmpty(Shapes.create(pEntity.getBoundingBox().move((double)(-pPos.getX()), (double)(-pPos.getY()), (double)(-pPos.getZ()))), pState.getShape(pLevel, pPos), BooleanOp.AND)) {
            var resourceKey = pLevel.dimension() == this.destinationDim ? Level.OVERWORLD : this.destinationDim;
            var serverLevel = ((ServerLevel)pLevel).getServer().getLevel(resourceKey);
            if (serverLevel == null) {
                return;
            }

            pEntity.changeDimension(serverLevel);
        }
    }
}
