package net.azagwen.dimension_mod.block;

import net.azagwen.dimension_mod.world.ModDimensions;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class LushPortalBlock extends AbstractPortalBlock {
    protected static final VoxelShape SHAPE = Block.box(0.0D, 0.0D, 0.0D, 16.0D, 10.0D, 16.0D);
    public static final IntegerProperty POS_X = IntegerProperty.create("pos_x", 0, 2);
    public static final IntegerProperty POS_Z = IntegerProperty.create("pos_z", 0, 2);

    public LushPortalBlock(Properties properties) {
        super(properties, ModDimensions.ORGANIC_REALM.getLevelKey());
    }

    @Override
    public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        return SHAPE;
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> pBuilder) {
        pBuilder.add(POS_X, POS_Z);
    }
}
