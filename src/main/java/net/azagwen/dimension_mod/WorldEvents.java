package net.azagwen.dimension_mod;

import net.azagwen.dimension_mod.block.BlockRegistry;
import net.azagwen.dimension_mod.events.RightClickBlockEvent;
import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.item.HoeItem;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.gameevent.GameEvent;

public class WorldEvents {

    public static void register() {

        // Moss hoe tilling event
        RightClickBlockEvent.register((context) -> {
            var stack = context.getItemInHand();

            if (stack.getItem() instanceof HoeItem) {
                var player = context.getPlayer();
                var world = context.getLevel();
                var pos = context.getClickedPos();
                var hand = context.getHand();
                var block = world.getBlockState(pos);

                if (player != null && block.is(Blocks.MOSS_BLOCK)) {
                    stack.hurtAndBreak(1, player, (p) -> p.broadcastBreakEvent(hand));
                    world.setBlock(pos, BlockRegistry.TILLED_MOSS_BLOCK.defaultBlockState(), 3);
                    world.playSound(player, pos, SoundEvents.HOE_TILL, SoundSource.BLOCKS, 1.0F, 1.0F);
                    player.swing(hand, world.isClientSide());
                    return InteractionResult.SUCCESS;
                }
            }
            return InteractionResult.PASS;
        });

        // Golden Carrot placement event
        RightClickBlockEvent.register((context) -> {
            var stack = context.getItemInHand();

            if (stack.is(Items.GOLDEN_CARROT)) {
                var player = context.getPlayer();
                var world = context.getLevel();
                var pos = context.getClickedPos();
                var hand = context.getHand();
                var block = world.getBlockState(pos);

                if (player != null && block.is(BlockRegistry.TILLED_MOSS_BLOCK) && context.getClickedFace() == Direction.UP && world.getBlockState(pos.above()).isAir()) {
                    var carrots = BlockRegistry.GOLDEN_CARROTS;
                    var carrotState = carrots.defaultBlockState();
                    var soundType = carrotState.getSoundType();
                    carrots.setPlacedBy(world, pos, carrotState, player, stack);

                    if (player instanceof ServerPlayer) {
                        CriteriaTriggers.PLACED_BLOCK.trigger((ServerPlayer)player, pos, stack);
                    }

                    world.gameEvent(GameEvent.BLOCK_PLACE, pos.above(), GameEvent.Context.of(player, carrotState));
                    world.setBlock(pos.above(), carrotState, 3);
                    world.playSound(player, pos, soundType.getPlaceSound(), SoundSource.BLOCKS, (soundType.getVolume() + 1.0F) / 2.0F, soundType.getPitch() * 0.8F);
                    player.swing(hand, world.isClientSide());
                    if (!player.getAbilities().instabuild) {
                        stack.shrink(1);
                    }
                    return InteractionResult.SUCCESS;
                }
            }
            return InteractionResult.PASS;
        });
    }
}
